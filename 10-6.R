#set working directory
setwd("c:/Users/Yaniv retyk/Desktop/ml")

#read the file into the data frame
spam.raw <- read.csv("spam.csv", stringsAsFactors = FALSE)

head(spam.raw)
View(spam.raw)
str(spam.raw)

spam <- spam.raw

#turne tipe into factor
spam$type <- as.factor(spam$type)

library(ggplot2)
ggplot(spam, aes(type)) + geom_bar()

#package tm
#install.packages('tm')
library(tm)

#create corpus
spam_corpus <- Corpus(VectorSource(spam$text))
View(spam_corpus)

clean_corpus[[1]][[1]]
clean_corpus <- tm_map(spam_corpus , removePunctuation)

#remove digits
clean_corpus <- tm_map(clean_corpus , removeNumbers)

#turn into lower case
clean_corpus <- tm_map(clean_corpus , content_transformer(tolower))

#remove stopwords
clean_corpus <- tm_map(clean_corpus , removeWords, stopwords())

#remove spaces
clean_corpus <- tm_map(clean_corpus , stripWhitespace)

dtm <- DocumentTermMatrix(clean_corpus)
dim(dtm)

#remove unfrequent attributes (words)
frequent_dtm <- DocumentTermMatrix(clean_corpus, list(dictionary = findFreqTerms(dtm, 10)))
dim(frequent_dtm)

install.packages('wordcloud')
library(wordcloud)

#color pallet
pal <- brewer.pal(9, 'Dark2')


wordcloud(clean_corpus[spam$type == 'spam'], min.freq = 5, random.order = FALSE, colors = pal)
wordcloud(clean_corpus[spam$type == 'ham'], min.freq = 5, random.order = FALSE, colors = pal)

#replace numbers with 'yes' or 'no'
convert_yesno <- function(x){
  if(x==0) return('no')
  return('yes')
}

yesno_matrix <- apply(frequent_dtm, MARGIN = 1:2, convert_yesno)

#convert to df
spam.df <- as.data.frame(yesno_matrix) 

str(spam.df)

#add the labels (target) column
spam.df$type <- spam$type
str(spam.df)
dim(spam.df)

randomvector <- runif(500) 
filter <- randomvector > 0.3

spam.df.train <- spam.df[filter,]
spam.df.test <- spam.df[!filter,]

dim(spam.df.train)
dim(spam.df.test)

#run naive base algorithm to generate a modle
install.packages('e1071')
library('e1071')

model <- naiveBayes(spam.df.train[,-60], spam.df.train$type)

#predicttion on the test set
prediction <- predict(model, spam.df.test[,-60], type = 'raw')

prediction_spam <- prediction[,'spam']
actual <- spam.df.test$type
predicted <- prediction_spam>0.7

#confusion matrix
conf_matrix <- table(predicted, actual)
conf_matrix

#compute precision and recall
#precision <- 
#recalol <- 

























































